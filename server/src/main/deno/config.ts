import { config } from "./deps.ts";

export const {
    MONGO_ATLAS_DATA_API_KEY,
    MONGO_ATLAS_APP_ID,
    MONGO_ATLAS_DATABASE,
} = config();
