export interface IYoutubeChannel {
    id: string;
    snippet: Snippet;
}

interface Snippet {
    title: string;
}
