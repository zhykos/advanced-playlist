export interface IProviderAuthDatabase {
    data: string[];
    provider: string;
}
